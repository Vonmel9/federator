'use strict';

let merge = require('webpack-merge')
let prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: JSON.stringify('development'),
  FIREBASE: {
    API_KEY: JSON.stringify('AIzaSyCdKtIILYucu3e8ypfZTFI9IHMywCu2mUE'),
    AUTH_DOMAIN: JSON.stringify('federator-e0fc5.firebaseapp.com'),
    DATABASE_URL: JSON.stringify('https://federator-e0fc5.firebaseio.com'),
    PROJECT_ID: JSON.stringify('federator-e0fc5'),
    STORAGE_BUCKET: JSON.stringify('federator-e0fc5.appspot.com'),
    MESSAGING_SENDER_ID: JSON.stringify('865843043218'),
  },
  FACEBOOK: {
    APP_ID: JSON.stringify('113901422540595'),
    APP_SECRET: JSON.stringify('e9900f9e1e37d2bb117be05a36ae4b74')
  }
})
