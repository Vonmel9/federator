import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user/store'
import modals from './modules/modals/store'
import flashMessage from './modules/flashMessage/store'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    modals,
    flashMessage
  }
})
