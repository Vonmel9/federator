import * as types from './types'
import firebase from 'firebase'

export default {
  [types.FETCH_USER_FROM_DATABASE] ({commit}, userID) {
    return firebase.database().ref('users/' + userID).once('value').then((snapshot) => {
      commit(types.SET_USER, snapshot.val())
    })
  },
  [types.LOGIN_TO_FACEBOOK] ({commit}, callback) {
    console.log('login to fb wanted')
    /* eslint-disable */
    FB.login((response) => {
      if (response.status === 'connected') {
        // Logged into your app and Facebook.
        console.log('connected ? => ' + response.status)
        // console.log(response)
      } else {
        // The person is not logged into this app or we are unable to tell.
        console.error('connected ? => ' + response.status)
        // console.log(response)
      }
      callback(response)
    })
    /* eslint-enable */
  },
  [types.CHECK_USER_LOGGED_IN_FACEBOOK] ({commit, dispatch}, callback) {
    /* eslint-disable */
    FB.getLoginStatus((response) => {
      if (response.status === 'connected') {
        console.log('Logged in.')
      }
      else {
        dispatch(types.LOGIN_TO_FACEBOOK, (response) => {
          console.log(response)
        })
      }
      callback(response)
    })
    /* eslint-enable */
  },
  [types.SET_HAS_ONE_FB_ACCOUNT_IN_FIREBASE] ({commit}, {user}) {
    // by default this prop is false, it toggle only if user add one fb account
    let updates = {}
    updates['users/' + user.id] = user

    return firebase.database().ref().update(updates)
  },
  [types.FETCH_USER_POSTS_FROM_FACEBOOK] ({commit}, {accessToken, targetURL, callBack}) {
    /* eslint-disable */
    FB.api(targetURL, ((response) => {
      console.log(response)
        callBack(response)
      }, { access_token: accessToken }) // TODO: accessToken is wrong in db !
    )
    /* eslint-enable */
  }
}
