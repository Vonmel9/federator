import * as types from './types'
import actions from './actions'

const state = {
  currentUser: null
}

const mutations = {
  [types.SET_USER] (state, user) {
    state.currentUser = user
  }
}

export default {
  state,
  mutations,
  actions
}
