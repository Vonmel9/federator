// mutations types
export const TOGGLE_ADD_SOCIAL_NETWORK_MODAL = 'modals/TOGGLE_ADD_SOCIAL_NETWORK_MODAL'
export const TOGGLE_CREATE_POST_MODAL = 'modals/TOGGLE_CREATE_POST_MODAL'

// actions types
export const ADD_SOCIAL_NETWORK = 'modals/ADD_SOCIAL_NETWORK'
export const PUBLISH_POST_ON_FB = 'modals/PUBLISH_POST_ON_FB'
