import * as types from './types'
import actions from './actions'

const state = {
  addSocialNetwork: false,
  createPost: false
}

const mutations = {
  [types.TOGGLE_ADD_SOCIAL_NETWORK_MODAL] (state) {
    state.addSocialNetwork = !state.addSocialNetwork
  },
  [types.TOGGLE_CREATE_POST_MODAL] (state) {
    state.createPost = !state.createPost
  }
}

export default {
  state,
  mutations,
  actions
}
