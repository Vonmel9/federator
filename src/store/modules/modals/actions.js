import * as types from './types'
import { SET_HAS_ONE_FB_ACCOUNT_IN_FIREBASE } from '../user/types'
import firebase from 'firebase'

export default {
  [types.ADD_SOCIAL_NETWORK] ({commit, dispatch}, {formData, user, rsCredentials}) {
    // create a new id (key in firebase language) for new social network
    let newSocialNetworkID = firebase.database().ref().child('users/' + user.id + '/social_networks').push().key

    let userHasOneFBAccount = false

    // set new social network data in an object
    let socialNetworkData = {
      id: newSocialNetworkID,
      name: formData.accountName,
      type: formData.accountType,
      post_per_page: 15,
      color: 'blue',
      icon: 'fa-' + formData.accountType + ' blue-text'
    }

    // if it's a facebook account, we add the two required keys in object which saved in firebase database
    if (formData.accountType === 'facebook') {
      socialNetworkData.fbAccessToken = rsCredentials.fbAccessToken
      socialNetworkData.fbUserID = rsCredentials.fbUserID

      userHasOneFBAccount = true
      user.hasOneFBAccount = userHasOneFBAccount
      dispatch(SET_HAS_ONE_FB_ACCOUNT_IN_FIREBASE, {user: user})
    }

    // if it's a twitter account, ...
    if (formData.accountType === 'twitter') {
      // TODO: add key / value for twitter account
    }

    // if it's an instagram account, ...
    if (formData.accountType === 'instagram') {
      // TODO: add key / value for instagram account
    }

    // create and use object updates which allow us to write the new data in the right user and social network
    let updates = {}
    updates['users/' + user.id + '/social_networks/' + newSocialNetworkID] = socialNetworkData
    console.log(updates)

    return firebase.database().ref().update(updates)
  },
  [types.PUBLISH_POST_ON_FB] ({commit}, {postMessage, targetURL, callBack}) {
    /* eslint-disable */
    FB.api(targetURL, 'post', {
        message: postMessage
      }, ((response) => {
        callBack(response)
      })
    )
    /* eslint-enable */
  }
}
