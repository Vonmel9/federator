import * as types from './types'

const state = {
  success: false,
  successMessage: '',
  error: false,
  errorMessage: ''
}

const mutations = {
  [types.TOGGLE_FLASH_MESSAGE_SUCCESS] (state) {
    state.success = !state.success
  },
  [types.SET_FLASH_MESSAGE_SUCCESS] (state, message) {
    state.successMessage = message
  },
  [types.TOGGLE_FLASH_MESSAGE_ERROR] (state) {
    state.error = !state.error
  },
  [types.SET_FLASH_MESSAGE_ERROR] (state, message) {
    state.errorMessage = message
  }
}

export default {
  state,
  mutations
}
