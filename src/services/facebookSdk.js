export default () => {
  let facebookAppID = process.env.FACEBOOK.APP_ID ? process.env.FACEBOOK.APP_ID : false
  if (facebookAppID) {
    /* eslint-disable */
    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk', 'FB'));
    /* eslint-enable */
  }
}
