// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App'
import router from './router'
import store from './store/index'
import { sync } from 'vuex-router-sync'
import firebase from 'firebase'
import Materials from 'vue-materials'
import initFacebookSdk from './services/facebookSdk'
// import initTwitterApi from './services/twitterApi'

import { FETCH_USER_FROM_DATABASE } from './store/modules/user/types'

Vue.use(VueResource)
Vue.use(Materials)

Vue.config.productionTip = false
Vue.config.devtools = true

// Initialize Firebase (use dev config if we are in dev environment, use prod config if we are in prod environment)
let config = {
  apiKey: process.env.FIREBASE.API_KEY,
  authDomain: process.env.FIREBASE.AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE.DATABASE_URL,
  storageBucket: process.env.FIREBASE.STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE.MESSAGING_SENDER_ID
}
firebase.initializeApp(config)

sync(store, router)

// initialisation of Facebook SDK
initFacebookSdk()
window.fbAsyncInit = () => {
  console.log('init fb sdk')
  /* eslint-disable */
  FB.init({
    appId            : process.env.FACEBOOK.APP_ID,
    autoLogAppEvents : true,
    xfbml            : true,
    version          : 'v2.9'
  })
  FB.AppEvents.logPageView()
  /* eslint-enable */
}

// initTwitterApi()

const unsuscribe = firebase.auth().onAuthStateChanged(user => {
  // Get user information in database thanks to user ID
  if (user !== null) {
    store.dispatch(FETCH_USER_FROM_DATABASE, user.uid)
      .then(() => {
        /* eslint-disable no-new */
        new Vue({
          el: '#app',
          router,
          store,
          render: h => h(App)
        })

        unsuscribe()
      })
  } else {
    /* eslint-disable no-new */
    new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App)
    })
  }
})
