import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/index'

import Layout from '../components/Layout/Layout'
import Login from '../components/Auth/Login'
import Register from '../components/Auth/Register'
import homeRoute from './home'

Vue.use(Router)

let mode = 'hash'

let routes = [
  {
    path: '/',
    component: Layout,
    children: [
      homeRoute
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  }
]

let router = new Router({routes, mode})

router.beforeEach((to, from, next) => {
  if ((to.path !== '/login' && store.state.user.currentUser === null) && to.path !== '/register') {
    next('/login')
  } else {
    next()
  }
})

export default router
