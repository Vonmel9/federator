import Home from '../components/Home/Home'

export default {
  path: '/',
  name: 'Home',
  component: Home
}
